package ro.tuc.ds2020.client.service;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonObject;

import ro.tuc.ds2020.client.ClientApplication;
import ro.tuc.ds2020.server.entities.auth.LoginResponse;


public class AuthService {

    private static AuthService INSTANCE;

    public static AuthService getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new AuthService();
        }
        return INSTANCE;
    }

    private AuthService() {
    }

    public LoginResponse login(String username, String password) throws IOException, InterruptedException {
        // create a client
        var client = HttpClient.newHttpClient();

        final JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("username", username);
        jsonObject.addProperty("password", password);

        // create a request
        var request = HttpRequest.newBuilder(
                        URI.create(ClientApplication.SERVER_URL + "/api/auth/signin"))
                .header("accept", "application/json")
                .header("Content-type", "application/json")
                .POST(HttpRequest.BodyPublishers.ofString(jsonObject.toString()))
                .build();

        // use the client to send the request
        final HttpResponse<LoginResponse> send = client.send(request, responseInfo -> {
            HttpResponse.BodySubscriber<String> upstream = HttpResponse.BodySubscribers.ofString(StandardCharsets.UTF_8);

            return HttpResponse.BodySubscribers.mapping(
                    upstream,
                    (String body) -> {
                        try {
                            ObjectMapper objectMapper = new ObjectMapper();
                            return objectMapper.readValue(body, LoginResponse.class);
                        } catch (IOException e) {
                            throw new UncheckedIOException(e);
                        }
                    });
        });

        return send.body();
    }
}
