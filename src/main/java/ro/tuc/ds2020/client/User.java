package ro.tuc.ds2020.client;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import ro.tuc.ds2020.client.rpc.RPCService;
import ro.tuc.ds2020.server.entities.auth.LoginResponse;


@RequiredArgsConstructor
@Getter
public class User {

    private final String username;
    private final String token;
    private static User INSTANCE;

    public static void setUser(LoginResponse loginResponse) {
        INSTANCE = new User(loginResponse.getUsername(), loginResponse.getToken());
        RPCService.getInstance().setHeader("Authorization", "Bearer " + loginResponse.getToken());
    }

    public static User getInstance() {
        return INSTANCE;
    }
}
