package ro.tuc.ds2020.client.rpc;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import com.googlecode.jsonrpc4j.JsonRpcHttpClient;

import lombok.SneakyThrows;
import ro.tuc.ds2020.client.ClientApplication;


class RPCClient {

    private final JsonRpcHttpClient rpcClient;
    private final Map<String, String> headers = new HashMap<>();

    @SneakyThrows
    protected RPCClient() {
        rpcClient = new JsonRpcHttpClient(new URL(ClientApplication.SERVER_URL + "/rpc"));
        headers.put("accept", "application/json");
    }

    public void setHeader(String key, String value) {
        headers.put(key, value);
    }

    protected <T> T invoke(String methodName, Object[] params, Class<T> clazz) throws Throwable {
        return rpcClient.invoke(methodName, params, clazz, headers);
    }
}
