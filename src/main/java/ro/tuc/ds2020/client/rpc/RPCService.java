package ro.tuc.ds2020.client.rpc;

import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

import ro.tuc.ds2020.server.dtos.BestStart;
import ro.tuc.ds2020.server.services.rpc.IRPCService;

@SuppressWarnings("unchecked")
public class RPCService extends RPCClient implements IRPCService {

    private static RPCService INSTANCE;

    public RPCService() {
        super();
    }

    public static RPCService getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new RPCService();
        }

        return INSTANCE;
    }

    @Override
    public List<UUID> getDevices() {
        try {
            final List<String> getDevices = invoke("getDevices", new Object[] {}, List.class);
            return getDevices.stream().map(UUID::fromString).collect(Collectors.toList());
        } catch (Throwable e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Map<String, Double> getHourlyEnergyConsumptionOverDays(int days) {
        try {
            return (Map<String, Double>) invoke("getHourlyEnergyConsumptionOverDays", new Object[] {days}, Map.class);
        } catch (Throwable e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public BestStart getBestStartTime(int hours) {
        try {
            return invoke("getBestStartTime", new Object[] {hours}, BestStart.class);
        } catch (Throwable e) {
            e.printStackTrace();
            return null;
        }
    }
}
