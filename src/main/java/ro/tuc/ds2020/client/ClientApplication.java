package ro.tuc.ds2020.client;

import javax.swing.*;

import ro.tuc.ds2020.client.ui.LoginPage;

public class ClientApplication {

    public static final String SERVER_URL = "https://ds-hdebreczeni-a3-rpc.herokuapp.com";

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            new LoginPage().setVisible(true);
        });
    }
}