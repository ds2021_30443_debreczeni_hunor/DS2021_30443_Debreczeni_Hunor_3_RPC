package ro.tuc.ds2020.client.ui;

import java.io.IOException;

import javax.swing.*;

import ro.tuc.ds2020.client.User;
import ro.tuc.ds2020.client.service.AuthService;


public class LoginPage extends JFrame {

    private final AuthService authService = AuthService.getInstance();
    private JTextField username;
    private JPanel mainPanel;
    private JPasswordField password;
    private JButton signInButton;

    public LoginPage() {
        this.setTitle(this.getClass().getSimpleName());
        this.setContentPane(this.mainPanel);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.pack();

        signInButton.addActionListener(e -> {
            try {
                User.setUser(authService.login(username.getText(), password.getText()));
                dispose();
                SwingUtilities.invokeLater(() -> {
                    new HomePage().setVisible(true);
                });
            } catch (IOException | InterruptedException ex) {
                JOptionPane.showMessageDialog(mainPanel, "IOException " + ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            }
        });
    }
}
