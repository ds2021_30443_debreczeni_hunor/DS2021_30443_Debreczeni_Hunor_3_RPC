package ro.tuc.ds2020.client.ui;

import java.awt.*;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

import javax.swing.*;

import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;

import ro.tuc.ds2020.client.rpc.RPCService;
import ro.tuc.ds2020.server.dtos.BestStart;

public class DevicePage extends JFrame {

    private JPanel mainPanel;
    private JScrollPane scrollPanel;
    private JSpinner nrHours;
    // private JSpinner deviceUsage;
    private final Map<String, Double> usageOverNDays;
    private final Map<String, Double> usageOver7Days;
    private final Map<String, Double> usageOver7DaysAvg;
    private BarChart customBarChart;

    public DevicePage(Map<String, Double> usageOverNDays, Map<String, Double> usageOver7Days) {
        this.usageOverNDays = usageOverNDays;
        this.usageOver7Days = usageOver7Days;
        usageOver7DaysAvg = new HashMap<>(usageOver7Days);
        usageOver7DaysAvg.forEach((key, value) -> usageOver7DaysAvg.put(key, value / 7));
        initUI();
    }

    private void initUI() {
        setTitle("Client data");
        getContentPane().setLayout(new BorderLayout());
        mainPanel = new JPanel();
        mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
        mainPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
        scrollPanel = new JScrollPane(mainPanel);
        scrollPanel.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        scrollPanel.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

        final BarChart comp = new BarChart("Energy Usage over selected days", "Hour", "Energy", createDataset(usageOverNDays));
        setPreferredSize(comp.getPreferredSize());
        mainPanel.add(comp);
        mainPanel.add(new BarChart("Energy Usage over 7 days (averaged)", "Hour", "Energy", createDataset(usageOver7DaysAvg)));

        nrHours = new JSpinner();
        nrHours.setMaximumSize(new Dimension(250, 200));
        nrHours.setBorder(BorderFactory.createTitledBorder("Number of hours/Duration"));
        // deviceUsage = new JSpinner();
        // deviceUsage.setMaximumSize(new Dimension(250, 200));
        // deviceUsage.setBorder(BorderFactory.createTitledBorder("Device energy usage"));
        mainPanel.add(nrHours);
        // mainPanel.add(deviceUsage);

        JButton calculateHours = new JButton();
        calculateHours.setText("calculateHours");
        calculateHours.addActionListener(e -> {
            // final Integer deviceUsageValue = (Integer) deviceUsage.getValue();
            final Integer nrHoursValue = (Integer) nrHours.getValue();
            if (nrHoursValue < 24 && nrHoursValue > 0) {
                if (customBarChart != null) {
                    mainPanel.remove(customBarChart);
                }

                final BestStart bestStartTime = RPCService.getInstance().getBestStartTime(nrHoursValue);
                customBarChart = new BarChart(
                        "Energy Usage over 7 days (CUSTOM with " + nrHoursValue + " hours)", "Hour", "Energy",
                        createDataset(getBestProgram(nrHoursValue, bestStartTime))
                );
                SwingUtilities.invokeLater(() -> {
                    mainPanel.add(customBarChart);
                });
                JOptionPane.showMessageDialog(this, "Custom chart calculated", "New chart", JOptionPane.INFORMATION_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(this, "Invalid parameters, please recheck", "Error", JOptionPane.ERROR_MESSAGE);
            }
        });

        mainPanel.add(calculateHours);

        add(scrollPanel, BorderLayout.CENTER);
        pack();
    }

    private CategoryDataset createDataset(Map<String, Double> data) {
        var dataset = new DefaultCategoryDataset();
        data.keySet().stream().sorted(Comparator.comparing(Integer::valueOf)).forEach(key -> {
            dataset.setValue(data.get(key), "Energy Usage", key);
        });
        return dataset;
    }

    private Map<String, Double> getBestProgram(int duration, BestStart bestStart) {
        final Map<String, Double> newMap = new HashMap<>(usageOver7DaysAvg);
        final int startHour = Integer.parseInt(bestStart.getStartHour());
        for (int i = 0; i < duration; i++) {
            newMap.computeIfPresent(String.valueOf((i + startHour) % 24), (key, value) -> value += bestStart.getUsage());
        }

        return newMap;
    }

}
