package ro.tuc.ds2020.client.ui;

import java.awt.*;
import java.util.UUID;

import javax.swing.*;

import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;

import ro.tuc.ds2020.client.User;
import ro.tuc.ds2020.client.rpc.RPCService;


public class HomePage extends JFrame {

    private JPanel mainPanel;
    private JSpinner nrDays;
    private JButton showDataButton;
    // private JList<UUID> deviceList;
    private JLabel usernameText;

    public HomePage() {
        initUI();
        initControllers();
    }

    private void initControllers() {
        final RPCService service = RPCService.getInstance();
        usernameText.setText("Hi, " + User.getInstance().getUsername());
        nrDays.setMinimumSize(new Dimension(80, 0));
        nrDays.setPreferredSize(new Dimension(80, 0));
        // nrDays.setMinimumSize(new Dimension(250, 0));
        nrDays.setBorder(BorderFactory.createTitledBorder("Nr days"));
        // deviceList.setBorder(BorderFactory.createTitledBorder("Select Device:"));
        // deviceList.setMinimumSize(new Dimension(300, 300));
        // deviceList.setPreferredSize(new Dimension(300, 300));
        setMinimumSize(new Dimension(400, 200));
        setPreferredSize(new Dimension(350, 400));
        mainPanel.setMinimumSize(new Dimension(350, 400));
        mainPanel.setPreferredSize(new Dimension(350, 400));
        showDataButton.addActionListener(e -> {
            // final UUID selectedValue = deviceList.getSelectedValue();
            final Integer value = (Integer)nrDays.getValue();
            if (value > 0) {
                SwingUtilities.invokeLater(() -> {
                    new DevicePage(
                            service.getHourlyEnergyConsumptionOverDays(value),
                            service.getHourlyEnergyConsumptionOverDays(7)
                    ).setVisible(true);
                });
            } else {
                JOptionPane.showMessageDialog(this, "Please select the data accordingly", "Invalid data", JOptionPane.ERROR_MESSAGE);
            }
        });
    }

    private void initUI() {
        setTitle(this.getClass().getSimpleName());
        setContentPane(this.mainPanel);
        // add(new BarChart("Energy Usage", "Hour", "Energy", createDataset()));
        pack();
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    private CategoryDataset createDataset() {
        var dataset = new DefaultCategoryDataset();
        dataset.setValue(46, "Gold medals", "USA");
        dataset.setValue(38, "Gold medals", "China");
        dataset.setValue(29, "Gold medals", "UK");
        dataset.setValue(22, "Gold medals", "Russia");
        dataset.setValue(13, "Gold medals", "South Korea");
        dataset.setValue(11, "Gold medals", "Germany");

        return dataset;
    }
}
