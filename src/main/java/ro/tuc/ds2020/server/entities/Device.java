package ro.tuc.ds2020.server.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;
import java.util.UUID;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Device extends HasId {

    private String name;
    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private User user;
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "device", cascade = CascadeType.REMOVE)
    private Set<Sensor> sensors;

    public Device(UUID id, String name, User user) {
        super(id);
        this.name = name;
        this.user = user;
    }
}
