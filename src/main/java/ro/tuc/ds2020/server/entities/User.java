package ro.tuc.ds2020.server.entities;

import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@Entity
@Table(name = "users_table")
@Getter
@Setter
public class User extends HasId implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column(nullable = false, unique = true)
    private String email;

    private String name;
    private String address;
    private Integer age;

    @Column(nullable = false)
    private String password;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "users_roles",
            joinColumns = @JoinColumn(
                    name = "user_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(
                    name = "role_id", referencedColumnName = "id"))
    private Set<Role> roles;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "user", cascade = CascadeType.REMOVE)
    private Set<Device> devices;

    public User() {
    }

    public User(UUID id, String email, String name, String address, Integer age) {
        super(id);
        this.email = email;
        this.name = name;
        this.address = address;
        this.age = age;
    }

    public List<GrantedAuthority> getGrantedAuthorities() {
        final List<GrantedAuthority> authorities = new ArrayList<>();
        for (final String privilege : getPrivileges()) {
            authorities.add(new SimpleGrantedAuthority(privilege));
        }
        return authorities;
    }

    public List<String> getPrivileges() {
        final List<String> privileges = new ArrayList<>();
        roles.forEach(role -> {
            privileges.add(role.getName());
            role.getPrivileges().forEach(privilege -> {
                privileges.add(privilege.getName());
            });
        });

        return privileges;
    }
}
