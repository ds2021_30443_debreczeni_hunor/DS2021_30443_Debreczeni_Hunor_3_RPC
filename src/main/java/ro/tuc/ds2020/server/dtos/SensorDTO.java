package ro.tuc.ds2020.server.dtos;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import ro.tuc.ds2020.server.entities.HasId;

import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
public class SensorDTO extends HasId {

    private String name;
    private UUID deviceId;
    private String deviceName;
    public SensorDTO(UUID id, String name, UUID deviceId, String deviceName) {
        super(id);
        this.name = name;
        this.deviceId = deviceId;
        this.deviceName = deviceName;
    }
}
