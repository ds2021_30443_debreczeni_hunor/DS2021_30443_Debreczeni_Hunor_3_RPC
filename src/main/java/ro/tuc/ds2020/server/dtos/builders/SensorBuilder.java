package ro.tuc.ds2020.server.dtos.builders;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.tuc.ds2020.server.dtos.SensorDTO;
import ro.tuc.ds2020.server.entities.Device;
import ro.tuc.ds2020.server.entities.Sensor;
import ro.tuc.ds2020.server.services.DeviceService;

@Component
public final class SensorBuilder implements EntityBuilder<SensorDTO, Sensor> {

    @Autowired
    private DeviceService deviceService;

    @Override
    public SensorDTO toDto(Sensor sensor) {
        return new SensorDTO(
                sensor.getId(),
                sensor.getName(),
                sensor.getDevice().getId(),
                sensor.getDevice().getName()
        );
    }

    @Override
    public Sensor toEntity(SensorDTO sensorDTO) {
        final Device device = deviceService.findObjById(sensorDTO.getDeviceId()).orElseThrow(() -> new IllegalArgumentException("Device not found"));

        return new Sensor(
                sensorDTO.getDeviceId(),
                sensorDTO.getName(),
                device
        );
    }

    @Override
    public Sensor toEntity(Sensor original, SensorDTO sensorDTO) {
        setIfPresent(sensorDTO::getName, original::setName);
        final Device device = deviceService.findObjById(sensorDTO.getDeviceId()).orElseThrow(() -> new IllegalArgumentException("Device not found"));
        original.setDevice(device);

        return original;
    }
}
