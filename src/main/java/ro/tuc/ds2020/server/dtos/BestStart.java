package ro.tuc.ds2020.server.dtos;

import lombok.Data;

@Data
public class BestStart {
    private String startHour;
    private Double usage;
}
