package ro.tuc.ds2020.server.dtos.builders;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import ro.tuc.ds2020.server.dtos.UserDTO;
import ro.tuc.ds2020.server.entities.HasId;
import ro.tuc.ds2020.server.entities.Role;
import ro.tuc.ds2020.server.entities.User;
import ro.tuc.ds2020.server.repositories.RoleRepository;

import java.util.HashSet;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public final class UserBuilder implements EntityBuilder<UserDTO, User> {


    private final PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
    @Autowired
    private RoleRepository roleRepository;

    public UserDTO toDto(User user) {
        return new UserDTO(
                user.getId(),
                user.getEmail(),
                user.getName(),
                user.getAddress(),
                user.getAge(),
                Optional.ofNullable(user.getDevices()).map(set ->
                        set.stream().map(HasId::getId).collect(Collectors.toSet())
                ).orElseGet(HashSet::new),
                user.getRoles().stream().map(Role::getName).collect(Collectors.toSet())
        );
    }

    public User toEntity(UserDTO personDetailsDTO) {
        final User user = new User(
                personDetailsDTO.getId(),
                personDetailsDTO.getEmail(),
                personDetailsDTO.getName(),
                personDetailsDTO.getAddress(),
                personDetailsDTO.getAge()
        );
        user.setPassword(passwordEncoder.encode(personDetailsDTO.getPassword()));
        user.setRoles(personDetailsDTO.getRoles().stream().map((String name) -> roleRepository.findByName(name).orElseThrow()).collect(Collectors.toSet()));
        return user;
    }

    @Override
    public User toEntity(User original, UserDTO userDTO) {
        setIfPresent(userDTO::getEmail, original::setEmail);
        setIfPresent(userDTO::getName, original::setName);
        setIfPresent(userDTO::getAddress, original::setAddress);
        setIfPresent(userDTO::getAge, original::setAge);
        if (userDTO.getPassword() != null) {
            original.setPassword(passwordEncoder.encode(userDTO.getPassword()));
        }
        if (userDTO.getRoles() != null) {
            original.setRoles(userDTO.getRoles().stream().map((String name) -> roleRepository.findByName(name).orElseThrow()).collect(Collectors.toSet()));
        }

        return original;
    }
}
