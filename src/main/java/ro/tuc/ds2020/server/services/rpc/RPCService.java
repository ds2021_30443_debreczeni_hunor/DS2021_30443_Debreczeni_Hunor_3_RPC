package ro.tuc.ds2020.server.services.rpc;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.googlecode.jsonrpc4j.spring.AutoJsonRpcServiceImpl;

import ro.tuc.ds2020.server.dtos.BestStart;
import ro.tuc.ds2020.server.entities.Device;
import ro.tuc.ds2020.server.entities.MonitoredData;
import ro.tuc.ds2020.server.entities.Sensor;
import ro.tuc.ds2020.server.entities.User;
import ro.tuc.ds2020.server.services.DeviceService;
import ro.tuc.ds2020.server.services.UserService;


@Service
@AutoJsonRpcServiceImpl
public class RPCService implements IRPCService {

    @Autowired
    private UserService userService;
    @Autowired
    private DeviceService deviceService;

    @Override
    public List<UUID> getDevices() {
        final Optional<User> userOptional = userService.findByEmail(SecurityContextHolder.getContext().getAuthentication().getName());
        if (userOptional.isEmpty()) {
            return null;
        }

        final User user = userOptional.get();
        return user.getDevices().stream().map(Device::getId).collect(Collectors.toList());
    }

    @Override
    public Map<String, Double> getHourlyEnergyConsumptionOverDays(int days) {
        final Optional<User> userOptional = userService.findByEmail(SecurityContextHolder.getContext().getAuthentication().getName());
        if (userOptional.isEmpty()) {
            return null;
        }

        final User user = userOptional.get();
        final Date date = new Date();
        final Long from = date.getTime() - TimeUnit.DAYS.toMillis(days);
        final Set<Device> allObj = user.getDevices();
        final Map<String, Double> data = new HashMap<>();
        for (Device device : allObj) {
            for (Sensor sensor : device.getSensors()) {
                sensor.getDataSet()
                        .stream()
                        .filter(sensorData -> sensorData.getTimestamp() > from)
                        .forEach(sensorData -> data.merge(String.valueOf(new Date(sensorData.getTimestamp()).getHours()),
                                sensorData.getMeasurement_value(), Double::sum
                        ));
            }
        }

        for (int i = 0; i < 24; i++) {
            data.putIfAbsent(String.valueOf(i), 0D);
        }

        return data;
    }

    @Override
    public BestStart getBestStartTime(int hours) {
        final Optional<User> userOptional = userService.findByEmail(SecurityContextHolder.getContext().getAuthentication().getName());
        if (userOptional.isEmpty()) {
            return null;
        }

        final User user = userOptional.get();
        Double max = 0D;
        final Set<Device> allObj = user.getDevices();
        for (Device device : allObj) {
            for (Sensor sensor : device.getSensors()) {
                for (MonitoredData data : sensor.getDataSet()) {
                    if (data.getMeasurement_value() > max) {
                        max = data.getMeasurement_value();
                    }
                }
            }
        }
        final Map<String, Double> hourlyEnergyConsumptionOverDays = getHourlyEnergyConsumptionOverDays(7);
        int startHour = 0;
        double currentMin = Double.MAX_VALUE;
        for (int i = 0; i < 24; i++) {
            double currentMax = 0;
            for (int k = 0; k < hours; k++) {
                final double newValue = hourlyEnergyConsumptionOverDays.get(String.valueOf((i + k) % 24)) + max;
                if (newValue > currentMax) {
                    currentMax = newValue;
                }
            }

            if (currentMin > currentMax) {
                currentMin = currentMax;
                startHour = i;
            }
        }

        final BestStart bestStart = new BestStart();
        bestStart.setStartHour(String.valueOf(startHour));
        bestStart.setUsage(max);
        return bestStart;
    }
}
