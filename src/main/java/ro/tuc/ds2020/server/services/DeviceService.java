package ro.tuc.ds2020.server.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.server.dtos.DeviceDTO;
import ro.tuc.ds2020.server.dtos.builders.DeviceBuilder;
import ro.tuc.ds2020.server.entities.Device;
import ro.tuc.ds2020.server.repositories.DeviceRepository;

@Service
public class DeviceService extends CrudService<DeviceDTO, Device> {

    @Autowired
    public DeviceService(DeviceBuilder deviceBuilder, DeviceRepository deviceRepository) {
        super(deviceBuilder, deviceRepository);
    }
}
