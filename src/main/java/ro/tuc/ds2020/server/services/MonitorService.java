package ro.tuc.ds2020.server.services;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.server.dtos.MonitoredDataDTO;
import ro.tuc.ds2020.server.dtos.builders.MonitoredDataBuilder;
import ro.tuc.ds2020.server.entities.MonitoredData;

import java.util.UUID;

@Service
public class MonitorService extends CrudService<MonitoredDataDTO, MonitoredData> {
    public MonitorService(MonitoredDataBuilder entityBuilder, JpaRepository<MonitoredData, UUID> repository) {
        super(entityBuilder, repository);
    }
}
