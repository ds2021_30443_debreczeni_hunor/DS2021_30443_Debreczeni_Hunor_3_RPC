package ro.tuc.ds2020.server.services.rpc;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.googlecode.jsonrpc4j.JsonRpcParam;
import com.googlecode.jsonrpc4j.JsonRpcService;

import ro.tuc.ds2020.server.dtos.BestStart;

@JsonRpcService("/rpc")
public interface IRPCService {

    List<UUID> getDevices();
    Map<String, Double> getHourlyEnergyConsumptionOverDays(int days);
    BestStart getBestStartTime(int hours);
}
