package ro.tuc.ds2020.server.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.server.dtos.SensorDTO;
import ro.tuc.ds2020.server.dtos.builders.SensorBuilder;
import ro.tuc.ds2020.server.entities.Sensor;
import ro.tuc.ds2020.server.repositories.SensorRepository;

@Service
public class SensorService extends CrudService<SensorDTO, Sensor> {
    @Autowired
    public SensorService(SensorBuilder sensorBuilder, SensorRepository sensorRepository) {
        super(sensorBuilder, sensorRepository);
    }
}
